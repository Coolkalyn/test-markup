var app = angular.module("app", ['ui.router', 'ngStorage', 'ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.run([
    '$rootScope', '$state', '$stateParams', 'SessionService',
    function($rootScope, $state, $stateParams, SessionService) {

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.user = null;

        // Здесь мы будем проверять авторизацию
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                SessionService.checkAccess(event, toState, toParams, fromState, fromParams);
            }
        );
    }
]);

(function () {
    'use strict';

    angular
        .module('app')
        .controller('PageController', PageController);
    PageController.$inject = ['$scope', '$sessionStorage', 'DataService', '$state'];
    function PageController($scope, $sessionStorage, DataService, $state) {

        //CheckforAdmin
        $scope.isAdmin = function() {
            return DataService.isAdmin();
        };

        //getCurrentUser
        $scope.getCurrentUser = function() {
            return $sessionStorage.user
        };

        //Logout
        $scope.logOut = function() {
            DataService.logout().then(function(response) {
                if (angular.isDefined(response.data.result) && response.data.result==1) {
                    delete $sessionStorage.user;
                    $state.go('/');
                }
            });
        };
    }
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);
    HomeController.$inject = ['$scope', '$sessionStorage', 'DataService', '$state'];
    function HomeController($scope, $sessionStorage, DataService, $state) {
        $scope.user = {};

        function userIn(user, method) {
            DataService[method](user).then(function(response) {
                if (response.data.result==1) {
                    $sessionStorage.user = response.data.data;
                    $scope.errors = '';
                    $state.go('Contact.list', {limit: 5, page: 1});
                } else {
                    if (angular.isDefined(response.data.error)) {
                        $scope.errors = response.data.error;
                    } else {
                        alert("Повторите ввод");
                    }
                }
            }, function(error) {
                $scope.errors = error.data.error;
            });
        }

        //Login
        $scope.login = function(user) {
            userIn(user, 'login');
        };

        //Register
        $scope.register = function(user) {
            userIn(user, 'register');
        };

    }
})();
(function () {
    'use strict';

    angular
        .module('app')
        .controller('ListController', ListController);
    ListController.$inject = ['$scope', '$stateParams', 'DataService', '$state', '$sessionStorage', '$uibModal'];
    function ListController($scope, $stateParams, DataService, $state, $sessionStorage, $uibModal) {

        $scope.page = {
            "loading": true,
            "data": [],
            "total": 0,
            "current": $stateParams.page,
            "limit": $stateParams.limit,
            "arrow": {
            }
        };

        //parameters for get data
        $scope.parameters = {
            limit: $scope.page.limit,
            page: $scope.page.current,
            sort:{
                direction:'desc',
                field: '_id'
            },
            search: ''
        };

        $scope.loadPageData = function (parameters) {
            DataService.list( parameters ).then(function (response) {
                $scope.page.loading = false;
                if (!response.data.result) {
                    $scope.errors = response.data.errors;
                } else {
                    $scope.page.data = response.data.data;
                    $scope.page.total = response.data.total;
                    var route = 'Contact.list';
                    $state.transitionTo(route, {limit:$scope.page.limit, page:$scope.page.current}, {
                        location: true,
                        inherit: true,
                        relative: $state.$current,
                        notify: false
                    });
                }
            });
        };
        $scope.loadPageData($scope.parameters);

        /**
         * pagination for list
         */
        $scope.pageChanged = function() {

            $scope.parameters.page = $scope.page.current;

            $scope.loadPageData($scope.parameters);

        };

        /**
         * sortBy model by field
         */
        $scope.sortBy = function(field) {

            $scope.parameters.sort = {
                direction: $scope.parameters.sort.direction === 'desc' ? 'asc' : 'desc',
                field: field
            };

            // change arrows
            angular.forEach($scope.page.arrow, function(value, key) {
                $scope.page.arrow[key] = 'fa-sort';
            });

            //change arrow on which clicked
            $scope.page.arrow[field] = $scope.parameters.sort.direction === 'desc' ? 'fa-sort-desc' : 'fa-sort-asc';

            $scope.loadPageData($scope.parameters);

        };

        $scope.search = function(value) {

            $scope.parameters.search = value;

            $scope.loadPageData($scope.parameters);
        };

        $scope.openModal = function (method, id) {
            var modalInstance = $uibModal.open({
                templateUrl: '/templates/modals/modalUser.html',
                controller: 'EditController',
                size: 'md',
                resolve: {
                    ContactId:  function() {
                        return id;
                    },
                    Method:  function() {
                        return method;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (method != 'info') {
                    $scope.loadPageData($scope.parameters);
                }
            });
        };

        $scope.delete = function (contact) {
            if (confirm("Do you want to delete user " + contact.firstName + "?")) {
                DataService.deleteContact(contact._id).then(function() {
                    $scope.loadPageData($scope.parameters);
                });
            }
        };

    }
})();
(function () {
    'use strict';

    angular
        .module('app')
        .controller('EditController', EditController);
    EditController.$inject = ['$scope', 'DataService', 'ContactId', 'Method', '$uibModalInstance', '$sessionStorage', '$uibModal'];
    function EditController($scope, DataService, ContactId, Method, $uibModalInstance, $sessionStorage, $uibModal) {

        $scope.user = {};
        $scope.disabled = !(angular.isDefined(Method) && (Method == 'add' || Method == 'edit'));
        $scope.method = 'add';
        if (angular.isDefined(Method)) $scope.method=Method;

        //CheckforAdmin
        $scope.isAdmin = function() {
            return DataService.isAdmin();
        };

        $scope.findContact = function(contactId) {
            $scope.yourAcc = false;
            DataService.getContact(contactId).then(function(doc) {
                if (doc.data.result==1) {
                    doc.data.data.salary = parseInt(doc.data.data.salary);
                    if (doc.data.data.birthday) doc.data.data.birthday = new Date(doc.data.data.birthday);
                    $scope.user = doc.data.data;
                    if ($sessionStorage.user._id == $scope.user._id) {
                        $scope.yourAcc = true;
                    }
                }
            }, function(response) {
                alert(response);
            });
        };

        if (angular.isDefined(ContactId)) {
            $scope.findContact(ContactId)
        }

        $scope.submit = function(user) {
            DataService[$scope.method + 'Contact'](user).then(function(response) {
                if (response.data.result==1) {
                    $uibModalInstance.close();
                }
            }, function(error) {
                $scope.errors = error.data.error;
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.changePassword = changePassword;
        function changePassword(user) {
            var modalInstancePassword = $uibModal.open({
                templateUrl: '/templates/modals/modalPassword.html',
                controller: changePasswordCtrl,
                size: 'md'
            });

            modalInstancePassword.result.then(function (Data) {
                $scope.user.password = Data.newPassword
            });
        }

        function changePasswordCtrl($scope, $uibModalInstance) {

            $scope.cancel = cancel;
            $scope.save = save;

            function save(oldPassword, newPassword, confirmPassword) {
                if (newPassword === confirmPassword) {
                    DataService.checkPassword($sessionStorage.user._id, oldPassword).then(function(response) {
                        if (response.data.error) {
                            $scope.errors = response.data.error;
                        } else if (response.data.result == 1) {
                            $scope.errors = '';
                            $uibModalInstance.close({newPassword: newPassword});
                        }
                    }, function(error) {
                        $scope.errors = error.data.error;
                    });
                } else {
                    $scope.errors = 'New password should match with confirm';
                }
            }

            function cancel() {
                $uibModalInstance.dismiss('cancel');
            }
        }

    }
})();
app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('auth', {
            url: '/auth',
            abstract: true,
            template: '<ui-view>'
        })
        .state('/', {
            url: '/',
            templateUrl: 'templates/main.html',
            controller: "HomeController",
            data: {
                'noLogin': true
            }
        })
        .state('Contact', {
            abstract:     true,
            url:          '/contacts',
            template:     '<ui-view/>'
        })
        .state('Contact.list', {
            url:            '/{limit:int}/{page:int}',
            templateUrl:    'templates/list.html',
            controller:     'ListController'
        });
    $urlRouterProvider.otherwise('/');
}]);
app.service('SessionService', [
    '$injector',
    function($injector) {
        "use strict";

        this.checkAccess = function(event, toState, toParams, fromState, fromParams) {
            var $scope = $injector.get('$rootScope'),
                $sessionStorage = $injector.get('$sessionStorage');

            if (toState.data !== undefined) {
                if (toState.data.noLogin !== undefined && toState.data.noLogin) {
                    if ($sessionStorage.user) {
                        event.preventDefault();
                        $scope.$state.go('Contact.list', {limit: 5, page: 1});
                    }
                }
            } else {
                // вход с авторизацией
                if ($sessionStorage.user) {
                    $scope.$root.user = $sessionStorage.user;
                } else {
                    // если пользователь не авторизован - отправляем на страницу авторизации
                    event.preventDefault();
                    $scope.$state.go('/');
                }
            }
        };
    }
]);

(function(){

    angular.module('app')
        .factory('DataService', DataService);
    DataService.$inject = [ '$http', '$sessionStorage'];
    function DataService( $http, $sessionStorage ){
        var DataService = {
            list            : list,
            login           : login,
            register        : register,
            logout          : logout,
            getContact      : getContact,
            addContact      : addContact,
            editContact     : editContact,
            deleteContact   : deleteContact,
            isAdmin         : isAdmin,
            checkPassword   : checkPassword
        };

        function list(parameters) {

            var value = {};

            if(angular.isDefined(parameters.sort)){
                value.sort = parameters.sort;
            }

            if(angular.isDefined(parameters.search)){
                value.search = parameters.search;
            }

            var limit = (parameters.limit === 0)?parameters.limit:(parameters.limit||10),
                page  = parameters.page||1;

            return $http.post("/contacts" + '/list/' + limit + '/' + page, value);
        }

        function login(user) {
            return $http.post("/auth/login", user);
        }

        function register(user) {
            return $http.post("/auth/register", user);
        }

        function logout() {
            return $http.get("/auth/logout");
        }

        function getContact(contactId) {
            return $http.get("/contacts/" + contactId);
        }

        function addContact(contact) {
            return $http.post("/contacts", contact);
        }

        function editContact(contact) {
            return $http.put("/contacts/" + contact._id, contact);
        }

        function deleteContact(contactId) {
            return $http.delete("/contacts/" + contactId);
        }

        function isAdmin() {
            return (angular.isDefined($sessionStorage.user) && $sessionStorage.user.role == "admin");
        }

        function checkPassword(Id, password) {
            return $http.post("/contacts/checkPassword/" + Id, {password: password});
        }

        return DataService;
    }
})();

(function(){

    var app 		= angular.module('app');

    /**
     *
     *	ApiInterceptor
     *		- Checks each outbound router for :api and replaces it with the correct API path
     *
     *
     **/
    app.factory('ApiInterceptor', [ '$sessionStorage', '$location' , function( $sessionStorage, $location ){
        return {

            'responseError': function( request ){

                if( request.status == 401 ){

                    delete $sessionStorage.user;
                    $location.go('/');

                }

                return request;

            }

        }
    }]);







    /**
     *
     *	$httpProvider.interceptors.push
     *		- Initializes the previous ApiInterceptor Factor
     *
     *
     **/
    app.config(['$httpProvider', function($httpProvider) {

        $httpProvider.interceptors.push('ApiInterceptor');

    }]);





})();
//# sourceMappingURL=app.js.map
