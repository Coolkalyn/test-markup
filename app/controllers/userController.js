var config = require('../../config.js');
var Users = require('../models/user');
var bCrypt = require('bcrypt-nodejs');

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
    res.status(code || 500).json({
        "error": message,
        "message": reason
    });
}

function createHash(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

/**
 * Get Paged contacts with sort and search filters
 */
module.exports.getPaged = function (req, res) {
    var sort = {};
    var direction = 'asc';
    if (req.body.sort) {
        direction = (req.body.sort.direction == 'desc' ? -1 : 1);
        sort[req.body.sort.field] = direction;
    }

    var limit = 5;
    var skip = 0;
    if (req.params.limit) {
        limit = parseInt(req.params.limit);
        if (req.params.page) {
            var page = parseInt(req.params.page);
            skip = limit * (page-1);
        }
    }

    var search = {};
    if (req.body.search) {
        search = {$or: [
            {firstName: {'$regex': req.body.search, $options: 'i'}},
            {secondName: {'$regex': req.body.search, $options: 'i'}}
        ]};
    }

    var cursor = Users.find({
        $and: [
            {role: 'user'},
            search
        ]
    });
    Users.find({
        $and: [
            {role: 'user'},
            search
        ]
    }).sort( sort ).skip(skip).limit(limit).exec(function(err, contacts) {
        if (err) {
            handleError(res, err.message, "Failed to get contacts.");
        } else {
            cursor.count(function(err, count){
                res.status(200).json({result: 1, data: contacts, total: count});
            });
        }
    });
};

/**
 * Find user by id
 */
module.exports.getById = function(req, res) {
    Users.findOne({
        _id: req.params.id
    }, function(err, doc) {
        if (err) {
            handleError(res, err.message, "Failed to get contact");
        } else {
            res.status(200).json({result: 1, data: doc});
        }
    });
};

/**
 * Add new user
 */
module.exports.add =  function(req, res) {
    var Contact = req.body;
    if (!(req.body.firstName || req.body.email || req.body.password)) {
        handleError(res, "Invalid user input", "Must provide all required datas.", 400);
    } else {
        Users.findOne({email: Contact.email}, function (err, user) {
            if (err) {
                handleError(res, "Invalid user input", "Smth goes wrong.", 400);
            }
            if (!user) {
                Contact.role = "user";
                Contact.password = createHash(Contact.password);
                Users.create(Contact, function (err, doc) {
                    if (err) {
                        handleError(res, err.message, "Failed to create new contact.");
                    } else {
                        res.status(200).json({result: 1, data: Contact});
                    }
                });
            } else {
                handleError(res, "Invalid user input", "This email is taken.", 400);
            }
        });
    }
};

/**
 * Edit user
 */
module.exports.edit = function(req, res) {
    var updateDoc = req.body;
    Users.findOne({email: updateDoc.email}, function (err, user) {
        if (err) {
            handleError(res, "Invalid user input", "Smth goes wrong.", 400);
        }
        if (!user || req.params.id == user._id) {
            delete updateDoc._id;
            updateDoc.password = createHash(updateDoc.password);
            Users.updateOne({
                _id: req.params.id
            }, updateDoc, function(err, doc) {
                if (err) {
                    handleError(res, err.message, "Failed to update contact");
                } else {
                    res.status(200).json({result: 1, data: doc});
                }
            });
        } else {
            handleError(res, "Invalid user input", "This email is taken.", 400);
        }
    });
};

/**
 * Delete user
 */
module.exports.delete = function(req, res) {
    Users.deleteOne({
        _id: req.params.id
    }, function(err, result) {
        if (err) {
            handleError(res, err.message, "Failed to delete contact");
        } else {
            res.status(204).end();
        }
    });
};

/**
 * Change password
 */
module.exports.checkPassword = function(req, res) {
    Users.findOne({
        _id: req.params.id
    }, function(err, user) {
        if (err) {
            handleError(res, err.message, "Failed to get contact");
        } else {
            bCrypt.compareSync(req.body.password, user.password) ?
                res.status(200).json({result: 1}) : handleError(res, "Wrong old password", "Wrong old password");
        }
    });
};