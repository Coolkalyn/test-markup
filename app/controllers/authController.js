var Users = require('../models/user');
var passport = require('passport');

var bCrypt = require('bcrypt-nodejs');

function createHash(password){
    return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
}

module.exports.login = function(req, res, next) {
    passport.authenticate('local',
        function(err, user, info) {
            return err
                ? next(err)
                : user
                ? req.logIn(user, function(err) {
                return err
                    ? next(err)
                    : res.status(200).json({result: 1, data: user});
            })
                : res.status(404).json({
                "error": "Wrong password"
            });
        }
    )(req, res, next);
};

module.exports.logout = function(req, res) {
    req.logout();
    res.status(200).json({result: 1});
};

module.exports.register = function(req, res, next) {
    var user = new Users({ email: req.body.email, password: createHash(req.body.password), firstName: req.body.firstName, role: "user"});
    user.save(function(err) {
        return err
            ? next(err)
            : req.logIn(user, function(err) {
            return err
                ? next(err)
                : res.status(200).json({result: 1, data: user});
        });
    });
};