var config = require('../../config.js'),
    express = require('express'),
    middleware = require('../middleware/auth.js'),
    User = require('../controllers/userController.js'),
    Auth = require('../controllers/authController.js');

module.exports = function (app) {
    app.post('/auth/login', Auth.login);
    app.post('/auth/register', Auth.register);
    app.get('/auth/logout', Auth.logout);

    app.post('/contacts/list/:limit/:page', middleware.mustAuthenticatedMw, User.getPaged);
    app.get('/contacts/:id', middleware.mustAuthenticatedMw, User.getById);
    app.post('/contacts/', middleware.mustAuthenticatedMw, User.add);
    app.put('/contacts/:id', middleware.mustAuthenticatedMw, User.edit);
    app.delete('/contacts/:id', middleware.mustAuthenticatedMw, User.delete);
    app.post('/contacts/checkPassword/:id', middleware.mustAuthenticatedMw, User.checkPassword);
};