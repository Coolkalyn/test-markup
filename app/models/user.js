var mongoose = require('mongoose');
    Schema = mongoose.Schema;

var User = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    secondName: {
        type: String
    },
    position: {
        type: String,
        enum: ['Programmer', 'Analyst', 'Enginer', 'Architect']
    },
    salary: {
        type: Number
    },
    phoneNumber: {
        type: String
    },
    birthday: {
        type: Date,
        default: null
    },
    role: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('contacts', User);