require('rootpath')();
var express = require('express');
var app = express();
var http = require('http').Server(app);
var port = process.env.PORT || 3000;
var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var config = require('./config');
var mongoose = require('mongoose');
var path = require('path');
var Users = require('./app/models/user');
var bCrypt = require('bcrypt-nodejs');

app.use(express.static(__dirname + '/public'));

app.use(cookieParser());
app.use(bodyParser());
app.use(session({ secret: config.secret }));

var passport       = require('passport');
var LocalStrategy  = require('passport-local').Strategy;

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});

passport.use(new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, function(username, password,done){
    Users.findOne({ email : username},function(err,user){
        return err
            ? done(err)
            : user
            ? bCrypt.compareSync(password, user.password)
            ? done(null, user)
            : done(null, false, { message: 'Incorrect password.' })
            : done(null, false, { message: 'Incorrect username.' });
    });
}));

// Passport:
app.use(passport.initialize());
app.use(passport.session());

/** ROUTES **/
require('./app/routes/')(app, {});

mongoose.connect(config.connectionString, {useMongoClient: true}).then(function (db) {
    if (db) {
        console.log('Connected to mongodb');
    }
});

http.listen(port, function() {
    console.log('listening on *:' + port);
});
