(function(){

    angular.module('app')
        .factory('DataService', DataService);
    DataService.$inject = [ '$http', '$sessionStorage'];
    function DataService( $http, $sessionStorage ){
        var DataService = {
            list            : list,
            login           : login,
            register        : register,
            logout          : logout,
            getContact      : getContact,
            addContact      : addContact,
            editContact     : editContact,
            deleteContact   : deleteContact,
            isAdmin         : isAdmin,
            checkPassword   : checkPassword
        };

        function list(parameters) {

            var value = {};

            if(angular.isDefined(parameters.sort)){
                value.sort = parameters.sort;
            }

            if(angular.isDefined(parameters.search)){
                value.search = parameters.search;
            }

            var limit = (parameters.limit === 0)?parameters.limit:(parameters.limit||10),
                page  = parameters.page||1;

            return $http.post("/contacts" + '/list/' + limit + '/' + page, value);
        }

        function login(user) {
            return $http.post("/auth/login", user);
        }

        function register(user) {
            return $http.post("/auth/register", user);
        }

        function logout() {
            return $http.get("/auth/logout");
        }

        function getContact(contactId) {
            return $http.get("/contacts/" + contactId);
        }

        function addContact(contact) {
            return $http.post("/contacts", contact);
        }

        function editContact(contact) {
            return $http.put("/contacts/" + contact._id, contact);
        }

        function deleteContact(contactId) {
            return $http.delete("/contacts/" + contactId);
        }

        function isAdmin() {
            return (angular.isDefined($sessionStorage.user) && $sessionStorage.user.role == "admin");
        }

        function checkPassword(Id, password) {
            return $http.post("/contacts/checkPassword/" + Id, {password: password});
        }

        return DataService;
    }
})();
