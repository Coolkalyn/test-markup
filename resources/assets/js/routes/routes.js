app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('auth', {
            url: '/auth',
            abstract: true,
            template: '<ui-view>'
        })
        .state('/', {
            url: '/',
            templateUrl: 'templates/main.html',
            controller: "HomeController",
            data: {
                'noLogin': true
            }
        })
        .state('Contact', {
            abstract:     true,
            url:          '/contacts',
            template:     '<ui-view/>'
        })
        .state('Contact.list', {
            url:            '/{limit:int}/{page:int}',
            templateUrl:    'templates/list.html',
            controller:     'ListController'
        });
    $urlRouterProvider.otherwise('/');
}]);