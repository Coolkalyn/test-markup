(function(){

    var app 		= angular.module('app');

    /**
     *
     *	ApiInterceptor
     *		- Checks each outbound router for :api and replaces it with the correct API path
     *
     *
     **/
    app.factory('ApiInterceptor', [ '$sessionStorage', '$location' , function( $sessionStorage, $location ){
        return {

            'responseError': function( request ){

                if( request.status == 401 ){

                    delete $sessionStorage.user;
                    $location.go('/');

                }

                return request;

            }

        }
    }]);







    /**
     *
     *	$httpProvider.interceptors.push
     *		- Initializes the previous ApiInterceptor Factor
     *
     *
     **/
    app.config(['$httpProvider', function($httpProvider) {

        $httpProvider.interceptors.push('ApiInterceptor');

    }]);





})();