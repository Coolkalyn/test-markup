(function () {
    'use strict';

    angular
        .module('app')
        .controller('EditController', EditController);
    EditController.$inject = ['$scope', 'DataService', 'ContactId', 'Method', '$uibModalInstance', '$sessionStorage', '$uibModal'];
    function EditController($scope, DataService, ContactId, Method, $uibModalInstance, $sessionStorage, $uibModal) {

        $scope.user = {};
        $scope.disabled = !(angular.isDefined(Method) && (Method == 'add' || Method == 'edit'));
        $scope.method = 'add';
        if (angular.isDefined(Method)) $scope.method=Method;

        //CheckforAdmin
        $scope.isAdmin = function() {
            return DataService.isAdmin();
        };

        $scope.findContact = function(contactId) {
            $scope.yourAcc = false;
            DataService.getContact(contactId).then(function(doc) {
                if (doc.data.result==1) {
                    doc.data.data.salary = parseInt(doc.data.data.salary);
                    if (doc.data.data.birthday) doc.data.data.birthday = new Date(doc.data.data.birthday);
                    $scope.user = doc.data.data;
                    if ($sessionStorage.user._id == $scope.user._id) {
                        $scope.yourAcc = true;
                    }
                }
            }, function(response) {
                alert(response);
            });
        };

        if (angular.isDefined(ContactId)) {
            $scope.findContact(ContactId)
        }

        $scope.submit = function(user) {
            DataService[$scope.method + 'Contact'](user).then(function(response) {
                if (response.data.result==1) {
                    $uibModalInstance.close();
                }
            }, function(error) {
                $scope.errors = error.data.error;
            });
        };

        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.changePassword = changePassword;
        function changePassword(user) {
            var modalInstancePassword = $uibModal.open({
                templateUrl: '/templates/modals/modalPassword.html',
                controller: changePasswordCtrl,
                size: 'md'
            });

            modalInstancePassword.result.then(function (Data) {
                $scope.user.password = Data.newPassword
            });
        }

        function changePasswordCtrl($scope, $uibModalInstance) {

            $scope.cancel = cancel;
            $scope.save = save;

            function save(oldPassword, newPassword, confirmPassword) {
                if (newPassword === confirmPassword) {
                    DataService.checkPassword($sessionStorage.user._id, oldPassword).then(function(response) {
                        if (response.data.error) {
                            $scope.errors = response.data.error;
                        } else if (response.data.result == 1) {
                            $scope.errors = '';
                            $uibModalInstance.close({newPassword: newPassword});
                        }
                    }, function(error) {
                        $scope.errors = error.data.error;
                    });
                } else {
                    $scope.errors = 'New password should match with confirm';
                }
            }

            function cancel() {
                $uibModalInstance.dismiss('cancel');
            }
        }

    }
})();