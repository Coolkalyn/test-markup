(function () {
    'use strict';

    angular
        .module('app')
        .controller('PageController', PageController);
    PageController.$inject = ['$scope', '$sessionStorage', 'DataService', '$state'];
    function PageController($scope, $sessionStorage, DataService, $state) {

        //CheckforAdmin
        $scope.isAdmin = function() {
            return DataService.isAdmin();
        };

        //getCurrentUser
        $scope.getCurrentUser = function() {
            return $sessionStorage.user
        };

        //Logout
        $scope.logOut = function() {
            DataService.logout().then(function(response) {
                if (angular.isDefined(response.data.result) && response.data.result==1) {
                    delete $sessionStorage.user;
                    $state.go('/');
                }
            });
        };
    }
})();
