(function () {
    'use strict';

    angular
        .module('app')
        .controller('ListController', ListController);
    ListController.$inject = ['$scope', '$stateParams', 'DataService', '$state', '$sessionStorage', '$uibModal'];
    function ListController($scope, $stateParams, DataService, $state, $sessionStorage, $uibModal) {

        $scope.page = {
            "loading": true,
            "data": [],
            "total": 0,
            "current": $stateParams.page,
            "limit": $stateParams.limit,
            "arrow": {
            }
        };

        //parameters for get data
        $scope.parameters = {
            limit: $scope.page.limit,
            page: $scope.page.current,
            sort:{
                direction:'desc',
                field: '_id'
            },
            search: ''
        };

        $scope.loadPageData = function (parameters) {
            DataService.list( parameters ).then(function (response) {
                $scope.page.loading = false;
                if (!response.data.result) {
                    $scope.errors = response.data.errors;
                } else {
                    $scope.page.data = response.data.data;
                    $scope.page.total = response.data.total;
                    var route = 'Contact.list';
                    $state.transitionTo(route, {limit:$scope.page.limit, page:$scope.page.current}, {
                        location: true,
                        inherit: true,
                        relative: $state.$current,
                        notify: false
                    });
                }
            });
        };
        $scope.loadPageData($scope.parameters);

        /**
         * pagination for list
         */
        $scope.pageChanged = function() {

            $scope.parameters.page = $scope.page.current;

            $scope.loadPageData($scope.parameters);

        };

        /**
         * sortBy model by field
         */
        $scope.sortBy = function(field) {

            $scope.parameters.sort = {
                direction: $scope.parameters.sort.direction === 'desc' ? 'asc' : 'desc',
                field: field
            };

            // change arrows
            angular.forEach($scope.page.arrow, function(value, key) {
                $scope.page.arrow[key] = 'fa-sort';
            });

            //change arrow on which clicked
            $scope.page.arrow[field] = $scope.parameters.sort.direction === 'desc' ? 'fa-sort-desc' : 'fa-sort-asc';

            $scope.loadPageData($scope.parameters);

        };

        $scope.search = function(value) {

            $scope.parameters.search = value;

            $scope.loadPageData($scope.parameters);
        };

        $scope.openModal = function (method, id) {
            var modalInstance = $uibModal.open({
                templateUrl: '/templates/modals/modalUser.html',
                controller: 'EditController',
                size: 'md',
                resolve: {
                    ContactId:  function() {
                        return id;
                    },
                    Method:  function() {
                        return method;
                    }
                }
            });

            modalInstance.result.then(function () {
                if (method != 'info') {
                    $scope.loadPageData($scope.parameters);
                }
            });
        };

        $scope.delete = function (contact) {
            if (confirm("Do you want to delete user " + contact.firstName + "?")) {
                DataService.deleteContact(contact._id).then(function() {
                    $scope.loadPageData($scope.parameters);
                });
            }
        };

    }
})();