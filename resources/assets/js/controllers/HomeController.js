(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);
    HomeController.$inject = ['$scope', '$sessionStorage', 'DataService', '$state'];
    function HomeController($scope, $sessionStorage, DataService, $state) {
        $scope.user = {};

        function userIn(user, method) {
            DataService[method](user).then(function(response) {
                if (response.data.result==1) {
                    $sessionStorage.user = response.data.data;
                    $scope.errors = '';
                    $state.go('Contact.list', {limit: 5, page: 1});
                } else {
                    if (angular.isDefined(response.data.error)) {
                        $scope.errors = response.data.error;
                    } else {
                        alert("Повторите ввод");
                    }
                }
            }, function(error) {
                $scope.errors = error.data.error;
            });
        }

        //Login
        $scope.login = function(user) {
            userIn(user, 'login');
        };

        //Register
        $scope.register = function(user) {
            userIn(user, 'register');
        };

    }
})();