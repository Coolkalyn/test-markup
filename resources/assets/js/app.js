var app = angular.module("app", ['ui.router', 'ngStorage', 'ngAnimate', 'ngSanitize', 'ui.bootstrap']);

app.run([
    '$rootScope', '$state', '$stateParams', 'SessionService',
    function($rootScope, $state, $stateParams, SessionService) {

        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;

        $rootScope.user = null;

        // Здесь мы будем проверять авторизацию
        $rootScope.$on('$stateChangeStart',
            function(event, toState, toParams, fromState, fromParams) {
                SessionService.checkAccess(event, toState, toParams, fromState, fromParams);
            }
        );
    }
]);
