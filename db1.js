
var MongoClient = require('mongodb').MongoClient
 , assert = require('assert');
var bCrypt = require('bcrypt-nodejs');
var createHash = function(password){
  return bCrypt.hashSync(password, bCrypt.genSaltSync(10), null);
};

// Connection URL
var url = 'mongodb://localhost:27017/test';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
    // Insert multiple documents
    db.collection('contacts').insertMany([
      {
        'email': "admin@admin",
        'password': createHash("admin"),
        'firstName': "Admin",
        'secondName': "Admin",
        'role': "admin"
      }, {
        'email': "jobmail1@gmail.com",
        'password': createHash("parol1"),
        'firstName': "Steve",
        'secondName': "Parker",
        'position': "Programmer",
        'salary': 100,
        'phoneNumber': "095-432-10-01",
        'birthday': "1992-12-09T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail2@gmail.com",
        'password': createHash("parol2"),
        'firstName': "Peter",
        'secondName': "Rogers",
        'position': "Programmer",
        'salary': 500,
        'phoneNumber': "095-432-10-02",
        'birthday': "1994-06-04T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail3@gmail.com",
        'password': createHash("parol3"),
        'firstName': "Helen",
        'secondName': "Smith",
        'position': "Programmer",
        'salary': 900,
        'phoneNumber': "095-432-10-03",
        'birthday': "1996-11-13T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail4@gmail.com",
        'password': createHash("parol4"),
        'firstName': "Steve",
        'secondName': "Stark",
        'position': "Programmer",
        'salary': 300,
        'phoneNumber': "095-432-10-04",
        'birthday': "1991-10-22T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail5@gmail.com",
        'password': createHash("parol5"),
        'firstName': "Tony",
        'secondName': "Stark",
        'position': "Programmer",
        'salary': 600,
        'phoneNumber': "095-432-10-05",
        'birthday': "1990-01-15T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail6@gmail.com",
        'password': createHash("parol6"),
        'firstName': "Peter",
        'secondName': "Parker",
        'position': "Programmer",
        'salary': 100,
        'phoneNumber': "095-432-10-01",
        'birthday': "1992-12-09T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail7@gmail.com",
        'password': createHash("parol7"),
        'firstName': "Steve",
        'secondName': "Rogers",
        'position': "Programmer",
        'salary': 500,
        'phoneNumber': "095-432-10-02",
        'birthday': "1994-06-04T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail8@gmail.com",
        'password': createHash("parol8"),
        'firstName': "Helen",
        'secondName': "Rogers",
        'position': "Programmer",
        'salary': 900,
        'phoneNumber': "095-432-10-03",
        'birthday': "1996-11-13T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail9@gmail.com",
        'password': createHash("parol9"),
        'firstName': "Helen",
        'secondName': "Stark",
        'position': "Programmer",
        'salary': 300,
        'phoneNumber': "095-432-10-04",
        'birthday': "1991-10-22T22:00:00.000Z",
        'role': "user"
      }, {
        'email': "jobmail10@gmail.com",
        'password': createHash("parol10"),
        'firstName': "Tony",
        'secondName': "Smith",
        'position': "Programmer",
        'salary': 600,
        'phoneNumber': "095-432-10-05",
        'birthday': "1990-01-15T22:00:00.000Z",
        'role': "user"
      }
    ], function(err, r) {
      assert.equal(null, err);
      assert.equal(11, r.insertedCount);
      console.log("inserted files!");
      db.close();
  });
});
