var elixir = require('laravel-elixir');
require('laravel-elixir-wiredep');

elixir(function(mix) {
    var assets = './resources/assets/';
    var config = {
        sassResources:      assets+'sass/',
        layoutsResources:   './public/',
        bowerDir:           './public/bower_components/'
    };

    mix.sass([
        'style.scss'
    ], 'public/css/style.css');

    //add all vendor js/css to master.blade.php
    mix.wiredep('html',{
        baseDir: config.layoutsResources,
        src: 'index.html'
    });

    //add all vendor scss to style.scss
    mix.wiredep('scss',{
        baseDir: config.sassResources,
        src: 'style.scss'
    });

    var appScripts = [
        'app.js',

        'controllers/PageController.js',
        'controllers/HomeController.js',
        'controllers/ListController.js',
        'controllers/EditController.js',

        'routes/routes.js',

        'services/auth.js',
        'services/DataService.js',

        'interceptors/http.js'
    ];

    mix.scripts(appScripts, 'public/js/app.js');

    mix.copy( config.sassResources + 'fonts/', 'public/fonts/' );
    mix.copy( config.bowerDir+'font-awesome/fonts/', 'public/fonts/' );
    mix.copy( config.bowerDir+'bootstrap-sass/assets/fonts/', 'public/fonts/' );
});